import transaction from './transaction';
import user from './user';
import wallet from './wallet';

export default (dps: any) => {
  return {
    user: user(dps),
    transaction: transaction(dps),
    wallet: wallet(dps),
  };
};
