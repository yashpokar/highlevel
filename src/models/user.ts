import { Model, DataTypes } from 'sequelize';

export default ({ db }: any) => {
  class User extends Model {
    public id!: string;
    public name!: string;

    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
  }

  User.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      // TODO :: create index
    },
  }, {
    sequelize: db,
    paranoid: true,
    tableName: 'users',
    underscored: true,
    indexes: [
      {
        name: 'name_idx',
        using: 'BTREE',
        fields: ['name'],
      }
    ]
  });

  return User;
};
