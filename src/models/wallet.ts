import { Model, DataTypes } from 'sequelize';

export default ({ db }: any) => {
  const User = db.models.User;

  class Wallet extends Model {
    public id!: string;
    public balance!: number;

    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
  }

  Wallet.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    balance: {
      type: DataTypes.DECIMAL(12, 2),
      allowNull: false,
    },
    userId: {
      type: DataTypes.UUID,
      allowNull: true,
      references: {
        model: User,
        key: 'id'
      }
    },
  }, {
    sequelize: db,
    paranoid: true,
    tableName: 'wallet',
    underscored: true,
  });

  Wallet.belongsTo(User);

  return Wallet;
};
