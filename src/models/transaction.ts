import { Model, DataTypes } from 'sequelize';

export default ({ db }: any) => {
  const User = db.models.User;

  class Transaction extends Model {
    public id!: string;
    public amount!: number;
    public type!: string;

    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
  }

  Transaction.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    amount: {
      type: DataTypes.DECIMAL(12, 2),
      allowNull: false,
    },
    description: {
      type: DataTypes.TEXT,
    },
    userId: {
      type: DataTypes.UUID,
      allowNull: true,
      references: {
        model: User,
        key: 'id'
      }
    },
  }, {
    sequelize: db,
    paranoid: true,
    tableName: 'transactions',
    underscored: true,
  });

  return Transaction;
};
