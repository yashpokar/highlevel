import express, { Application } from 'express';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import winston from 'winston';
import path from 'path';
import cors from 'cors';
import { Sequelize, Model, DataTypes } from 'sequelize';
import config from './config';

const app: Application = express();

const db = new Sequelize(config.database.connectionString);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors(config.cors));

const logger = winston.createLogger({
  level: config.log.level,
  format: winston.format.json(),
  defaultMeta: { service: 'user-service' },
  transports: [
    new winston.transports.File({ filename: path.resolve(__dirname, '../storage', 'logs', 'error.log'), level: 'error' }),
    new winston.transports.File({ filename: path.resolve(__dirname, '../storage', 'logs', 'combined.log') }),
  ],
});

import controller from './controllers';
import models from './models';

models({ config, db, logger });
controller({ app, db, logger });

export {
  app,
  config,
  db,
  logger,
};
