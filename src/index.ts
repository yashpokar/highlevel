import { config, app } from './app';

app.listen(config.port, () => {
  console.log(`Server has started at port ${config.port}`)
});
