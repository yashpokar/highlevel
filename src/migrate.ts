import { db } from './app';

(async () => {
  await db.sync({ alter: true });
})();
