import dotenv from 'dotenv-safe';

dotenv.config();

const source = process.env;

export default {
  port: source.PORT || 8000,
  database: {
    connectionString: 'mysql://root:root@localhost:3306/wallet',
  },
  cors: {
    origin: source.CORS_ALLOWED_DOMAINS,
    optionsSuccessStatus: 200,
    credentials: true,
  },
  log: {
    level: 'debug',
  },
};
