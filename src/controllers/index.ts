import wallet from './wallet';
import transaction from './transaction';

export default ({ app, ...dps }: any) => {
  app.use(wallet(dps));
  app.use(transaction(dps));
};
