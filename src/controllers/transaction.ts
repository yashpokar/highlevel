import { Router, Request, Response } from 'express';
import { body, param, query } from 'express-validator';

const router = Router();

export default ({ logger, db }: any) => {
  const User = db.models.User;
  const Transaction = db.models.Transaction;
  const Wallet = db.models.Wallet;

  router.post('/transact/:walletId', [
    body('amount')
      .notEmpty()
      .withMessage('Amount is required')
       .bail()
       .isNumeric()
       .bail(),

    body('description')
      .trim()
      .escape()
      .notEmpty()
      .withMessage('Description is required'),

    param('walletId')
      .notEmpty()
      .withMessage('Wallet id required')
      .bail()
      .isUUID()
      .withMessage('Wallet id is incorrect.'),
  ], async (req: Request, res: Response) => {
    const results: any = {};
    logger.info('recived request for credit/debit amount', req.body)
    const amount = req.body.amount;

    if (amount === 0) {
      return res.status(422).json({
            success: true,
            message: 'Validation error',
            errors: {'amount': 'Invalid amount.'}
          });
    }

    try {
      const transactionResult = await db.transaction(async (transaction: any) => {
        const wallet = await Wallet.findByPk(req.params.walletId, { transaction });

        if (! wallet) {
          return res.status(422).json({
            success: true,
            message: 'Validation error',
            errors: {'walletId': 'Wallet id does not exists.'}
          });
        }

        const balance = wallet.balance + amount;
        wallet.balance = balance;
        results.balance = balance;

        await wallet.save();

        const transac = await Transaction.create({
          amount,
          description: req.body.description,
          userId: wallet.userId,
        }, { transaction });
        results.transactionId = transac.id;
      });
    } catch (err) {
      logger.error('error occured while transacting amount', err);

      return res.status(500).json({
        success: false,
        message: 'Internal server error.',
      });
    }

    return res.json({
      success: true,
      message: 'Transaction made successfully.',
      ...results,
    });
  });

  router.get('/transactions', [
    query('walletId')
      .notEmpty()
      .withMessage('Wallet is required.')
      .isUUID()
      .withMessage('Wallet should be valid.')
      .bail(),

     query('limit')
       .optional()
       .bail()
       .isNumeric()
       .withMessage('Limit should be numeric'),

     query('skip')
       .optional()
       .bail()
       .isNumeric()
       .withMessage('Skip should be numeric'),
  ], async (req: Request, res: Response) => {
    const limit = Number(req.query.limit || '10');
    const offset = Number(req.query.skip || '0');

    console.log(req.query.walletId);

    const wallet = await Wallet.findByPk(req.query.walletId);

    if (! wallet) {
      return res.status(422).json({
        success: true,
        message: 'Validation error',
        errors: {'walletId': 'Wallet id does not exists.'}
      });
    }

    if (limit > 100) {
      return res.json({
        success: false,
        message: 'Only 100 documents can be retrieved in a single go',
      });
    }

    const rows = await Transaction.findAll({
      where: {
        userId: wallet.userId
      },
      offset,
      limit
    });

    return res.json({
      success: true,
      message: 'Retrived all the transactions successfully.',
      transactions: rows.map((row: any) => {
        return {
          id: row.id,
          wallet: wallet.Id,
          amount: row.amount,
          // TOOD :: this is not valid, so leaving off
            // I belive it denotes historicle balance
            // at time of that transaction
          // balance: row.amount,
          description: row.description,
          date: row.createdAt,
          type: row.amount > 0 ? 'CREDIT' : 'DEBIT'
        };
      }),
    });
  });

  return router;
};
