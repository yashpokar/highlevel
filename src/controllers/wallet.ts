import { Router, Request, Response } from 'express';
import { body, param } from 'express-validator';

const router = Router();

export default ({ logger, db }: any) => {
  const User = db.models.User;
  const Transaction = db.models.Transaction;
  const Wallet = db.models.Wallet;

  router.post('/setup', [
    // TODO :: ideally there should be a unique identify for the user,
      // which we need to validate that it is not being setup again
    body('name')
      .trim()
      .escape()
      .notEmpty()
      .withMessage('Name is required.')
      .bail()
      .isLength({ max: 100 })
      .withMessage('Name must be having less than 100 characters.'),

    body('balance')
       .notEmpty()
       .withMessage('Balance is required.')
       .bail()
       // TODO : many more rules can be applied like this
         // but because of shortage of time
       .isNumeric()
        .withMessage('Balance is invalid.'),
  ], async (req: Request, res: Response) => {
    const results: any = {};

    try {
      const transactionResult = await db.transaction(async (transaction: any) => {
        const user = await User.create({
          name: req.body.name,
        }, { transaction });

        results.name = user.name;

        const transac = await Transaction.create({
          amount: req.body.balance,
          // TODO :: couldn't clearify this
          description: 'First deposit',
          userId: user.id,
        }, { transaction });

        results.transactionId = transac.id;
        results.balance = transac.amount;
        results.transactionDate = transac.createdAt;

        const wallet = await Wallet.create({
          // TODO :: this make a problem, if we try setting up
            // an account multuple times with the same name
          balance: req.body.balance,
          userId: user.id,
        }, { transaction });

        results.id = wallet.id;
      });
    } catch (err) {
      logger.error('error occured while setting up the account %s', err);

      return res.status(500).json({
        success: false,
        message: 'Internal server error.',
      });
    }

    return res.json({
      success: true,
      message: 'Setup completed successfully.',
      ...results
    });
  });

  router.get('/wallet/:id', [
    param('id')
      .escape()
      .trim()
      .notEmpty()
      .withMessage('Wallet id is required.')
  ], async (req: Request, res: Response) => {
    // TODO :: wrap everything inside try/catch block
    const wallet = await Wallet.findOne({
          where: { id: req.params.id },
          include: [{ model: User }]
     });

    if (! wallet) {
      return res.status(422).json({
        success: true,
        message: 'Validation error',
        errors: {'walletId': 'Wallet id does not exists.'}
      });
    }

    return res.json({
      success: true,
      message: 'Fetched all the wallet information successfully.',
      id: wallet.id,
      balance: wallet.balance,
      name: wallet.User.name,
      date: wallet.createdAt,
    });
  });

  return router;
};
